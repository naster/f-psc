﻿using AutoMapper;
using FPSC.Data;
using FPSC.Data.DTO;
using FPSC.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FPSC.PizzaService
{
    public class PizzaRioService : IPizzaService
    {
        private readonly PizzaContext _context;
        private readonly IMapper _mapper;

        public PizzaRioService(PizzaContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public List<PizzaMenuItemDTO> GetMenu()
        {
            var menu = _context.PizzaMenuItem
                            .Include(item => item.PizzaSize)
                            .Include(item => item.PizzaType)
                        .Select(_mapper.Map<PizzaMenuItemDTO>);

            return menu.ToList();
        }

        public List<IGrouping<string, PizzaMenuItemDTO>> GetMenuGroupedByPizzaType()
        {
            var menuItemsGrouped = (from item in GetMenu()
                                    group item by item.PizzaTypeName into grp
                                    orderby grp.Key
                                    select grp).ToList();
            return menuItemsGrouped;
        }

        public ResultData<PizzaOrderDTO> GetPizzaOrder(int orderId)
        {
            throw new NotImplementedException();
        }

        public ResultData<PizzaOrderDTO> CreatePizzaOrder()
        {
            var pizzaOrder = new PizzaOrder();
            _context.PizzaOrder.Add(pizzaOrder);
            _context.SaveChanges();
            return new ResultData<PizzaOrderDTO>(_mapper.Map<PizzaOrderDTO>(pizzaOrder));
        }

        public Result AddPizzaUserOrder(int orderId, PizzaUserOrderDTO userOrder)
        {
            var pizzaOrderItem = from o in userOrder.OrderItems
                                 select new PizzaOrderItem
                                 {
                                     NumberOfSlices = o.NumberOfSlices,
                                     PizzaOrderId = orderId,
                                     PizzaTypeId = o.PizzaTypeId,
                                     UserName = userOrder.UserName
                                 };

            _context.PizzaOrderItem.AddRange(pizzaOrderItem);
            _context.SaveChanges();

            return new Result();
        }

        public List<PizzaTypeDTO> GetPizzaTypes()
        {
            var types = _context.PizzaType.Select(_mapper.Map<PizzaTypeDTO>);
            return types.ToList();
        }
    }
}
