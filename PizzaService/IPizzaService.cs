﻿using FPSC.Data.DTO;
using System.Collections.Generic;
using System.Linq;

namespace FPSC.PizzaService
{
    public interface IPizzaService
    {
        List<PizzaMenuItemDTO> GetMenu();
        List<IGrouping<string, PizzaMenuItemDTO>> GetMenuGroupedByPizzaType();
        List<PizzaTypeDTO> GetPizzaTypes();
        ResultData<PizzaOrderDTO> GetPizzaOrder(int orderId);
        ResultData<PizzaOrderDTO> CreatePizzaOrder();
        Result AddPizzaUserOrder(int orderId, PizzaUserOrderDTO userOrder);
    }
}