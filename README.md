# Familiprix F-PSC Challenge

### Le défi
Le défi, si tu l'acceptes, sera de développer une application web qui permet aux développeurs de Familiprix de passer une commande de pizza à leur pizzéria préférée.

### Prérequis
- Le projet doit être réalisé en C#, sous Visual Studio. Si tu as besoin de le télécharger, il est possible d'en obtenir une version [ici](https://code.visualstudio.com/) 
- "Fork" le répertoire actuel pour démarrer ton projet. Une fois celui-ci terminé, tu pourras nous fournir l'URL de ton répertoire GIT.

## Le projet

### Le "flow" d'une commande de pizza dans notre équipe est le suivant:
 - Une commande pour l'équipe est créée en date du jour
 - Un menu des pizzas est disponible pour consultation
 - Les membres de l'équipe s'ajoutent à la commande en y indiquant leur nom et le nombre de pointes qu'ils veulent pour les différentes sortes disponibles sur le menu.
 - La commande est finalisée et comptabilisée pour ensuite appeler au restaurant. Le montant total donné par le restaurant est divisé par le nombre de personnes dans la commande (avantage aux gloutons).

### Afin d'évaluer ton projet, nous nous attendons à y retrouver les éléments suivants:
 - L'utilisation de la base de données déjà présente dans le code. Elle a déjà un modèle de donnée minimale correspondant au "flow" décrit précédemment.
 - Une page web à partir de laquelle nous pourrons effectuer notre commande et consulter celle-ci
 - Une structure de classe qui nous permettra de maintenir et faire facilement évoluer le projet
 
### Bonus
 - Au besoin, tu peux modifier la base de données si le modèle ne te convient pas.
 - Tu peux créer autant de pages web que tu le désires, utiliser le framework front-end de ton choix ou décider d'effectuer l'intégration manuellement

## Conseils
- Montre-nous tes talents de développeur full-stack
- Sois à l'aise de modifier le projet, d'y ajouter des fonctionnalités et de sortir des sentiers battus.
- Ajoutes-y  des tests unitaires si tu le désires
- Garde en tête que ce projet sera mis à l'épreuve par notre équipe de développeurs. On veut que tu nous montres ce que tu fais de mieux, du code bien segmenté, propre et "bug-free".