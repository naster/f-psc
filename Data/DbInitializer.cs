﻿using FPSC.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FPSC.Data
{
    public class DbInitializer
    {
        public static void Initialize(PizzaContext context)
        {
            // Fully delete each time
            //context.Database.EnsureDeleted();

            context.Database.EnsureCreated();

            // Look for any pizza.
            if (context.PizzaMenuItem.Any())
            {
                return;   // DB has been seeded
            }

            PizzaType[] pizzaTypes = new PizzaType[]
            {
                new PizzaType(){ Name = "Meat lover"},
                new PizzaType(){ Name = "All dressed"}
            };

            context.PizzaType.AddRange(pizzaTypes);

            PizzaSize[] pizzaSize = new PizzaSize[]
            {
                new PizzaSize(){ Name = "Small", NumberOfSlices = 4},
                new PizzaSize(){ Name = "Medium", NumberOfSlices = 6},
                new PizzaSize(){ Name = "Large", NumberOfSlices = 8},
                new PizzaSize(){ Name = "XLarge", NumberOfSlices = 12},
            };

            context.PizzaSize.AddRange(pizzaSize);

            context.SaveChanges();

            var sizeDictionnary = new[] { "Small", "Medium", "Large", "XLarge" }.Aggregate(
                new Dictionary<string, int>(),
                (dic, sizeStr) => { dic.Add(sizeStr, context.PizzaSize.FirstOrDefault(s => s.Name == sizeStr).PizzaSizeId); return dic; });

            var typeDictionnary = new[] { "Meat lover", "All dressed" }.Aggregate(
                new Dictionary<string, int>(),
                (dic, typeStr) => { dic.Add(typeStr, context.PizzaType.FirstOrDefault(s => s.Name == typeStr).PizzaTypeId); return dic; });


            PizzaMenuItem[] menuItems = new PizzaMenuItem[]
            {
                new PizzaMenuItem(){PizzaSizeId = sizeDictionnary["Small"], PizzaTypeId = typeDictionnary["Meat lover"], Price= 8},
                new PizzaMenuItem(){PizzaSizeId = sizeDictionnary["Medium"], PizzaTypeId = typeDictionnary["Meat lover"], Price= 10},
                new PizzaMenuItem(){PizzaSizeId = sizeDictionnary["Large"], PizzaTypeId = typeDictionnary["Meat lover"], Price= 12},

                new PizzaMenuItem(){PizzaSizeId = sizeDictionnary["Small"], PizzaTypeId = typeDictionnary["All dressed"], Price= 9},
                new PizzaMenuItem(){PizzaSizeId = sizeDictionnary["Medium"], PizzaTypeId = typeDictionnary["All dressed"], Price= 11},
                new PizzaMenuItem(){PizzaSizeId = sizeDictionnary["Large"], PizzaTypeId = typeDictionnary["All dressed"], Price= 13},
            };

            context.PizzaMenuItem.AddRange(menuItems);

            context.SaveChanges();
        }
    }
}
