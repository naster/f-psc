using System;
using System.Collections.Generic;

public class Result{
    public List<string> Messages {get; private set;}
    public bool Success => Messages.Count == 0;
    public Result(){
        Messages = new List<string>();
    }

    public Result Trying(Action<List<string>> action){
        try
        {
            var actionMessages = new List<string>();
            action(actionMessages);
        }
        catch (Exception e)
        {
            Messages.Add(e.ToString());
        }
        return this;
    }

    public Result WithMessages(IEnumerable<string> messages){
        Messages.AddRange(messages);
        return this;
    }

    public Result WithMessages(string message){
        Messages.Add(message);
        return this;
    }
}

public class ResultData<T> : Result{
    public T Data { get; set;}

    public ResultData(T data) : base()
    {
        Data = data;
    }

    public ResultData(): base(){
        
    }

    public new ResultData<T> Trying(Action<List<string>> action){
        base.Trying(action);
        return this;
    }

    public ResultData<T> WithData(T data){
        Data = data;
        return this;
    }

    public new ResultData<T> WithMessages(IEnumerable<string> messages){
        base.WithMessages(messages);
        return this;
    }

    public new ResultData<T> WithMessages(string message){
        base.WithMessages(message);
        return this;
    }
}