﻿namespace FPSC.Data.DTO
{
    public class PizzaTypeDTO
    {
        public int PizzaTypeId { get; set; }
        public string Name { get; set; }
    }
}
