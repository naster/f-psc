using System;
using System.Collections.Generic;

namespace FPSC.Data.DTO{
    public class PizzaOrderDTO{
        public int PizzaOrderId { get; set; }
        public DateTime Date { get; set; }
        
        public List<PizzaUserOrderDTO> UserOrders { get; set; }
    }
}
