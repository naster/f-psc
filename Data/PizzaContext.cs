﻿using FPSC.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace FPSC.Data
{
    public class PizzaContext: DbContext
    {
        public PizzaContext(DbContextOptions<PizzaContext> options): base(options)
        {

        }

        public DbSet<PizzaMenuItem> PizzaMenuItem { get; set; }
        public DbSet<PizzaType> PizzaType { get; set; }
        public DbSet<PizzaSize> PizzaSize { get; set; }

        public DbSet<PizzaOrder> PizzaOrder {get; set;}
        public DbSet<PizzaOrderItem> PizzaOrderItem {get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PizzaMenuItem>().ToTable("PizzaMenuItem");
            modelBuilder.Entity<PizzaType>().ToTable("PizzaType");
            modelBuilder.Entity<PizzaSize>().ToTable("PizzaSize");
            modelBuilder.Entity<PizzaOrder>().ToTable("PizzaOrder");
            modelBuilder.Entity<PizzaOrderItem>().ToTable("PizzaOrderItem");
        }
    }
}
