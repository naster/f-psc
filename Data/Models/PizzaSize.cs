﻿namespace FPSC.Data.Models
{
    public class PizzaSize
    {
        public int PizzaSizeId { get; set; }
        public string Name { get; set; }
        public int NumberOfSlices { get; set; }
    }
}
