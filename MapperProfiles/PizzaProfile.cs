﻿using AutoMapper;
using FPSC.Data.DTO;
using FPSC.Data.Models;
using System.Linq;

namespace FPSC.MapperProfiles.Profiles
{
    public class PizzaProfile : Profile
    {
        public PizzaProfile()
        {
            CreateMap<PizzaMenuItem, PizzaMenuItemDTO>();
            CreateMap<PizzaOrderItem, PizzaUserOrderItemDTO>();
            CreateMap<PizzaOrder, PizzaOrderDTO>().ConvertUsing(PizzaOrderToDTO);
        }

        private PizzaOrderDTO PizzaOrderToDTO(PizzaOrder pizzaOrder, PizzaOrderDTO dto, ResolutionContext context)
        {
            var pizzaOrderDTO = new PizzaOrderDTO()
            {
                Date = pizzaOrder.Date,
                PizzaOrderId = pizzaOrder.PizzaOrderId,
            };

            if (pizzaOrder.PizzaOrderItems != null)
            {
                var orderByUser = from orderItem in pizzaOrder.PizzaOrderItems
                                  group orderItem by orderItem.UserName into grp
                                  select grp;

                foreach (var userOrders in orderByUser)
                {
                    var userOrder = new PizzaUserOrderDTO { UserName = userOrders.Key };
                    foreach (var orderItem in userOrders)
                    {
                        var userOrderItem = new PizzaUserOrderItemDTO
                        {
                            NumberOfSlices = orderItem.NumberOfSlices,
                            PizzaTypeId = orderItem.PizzaTypeId,
                            PizzaTypeName = orderItem.PizzaType.Name
                        };
                        userOrder.OrderItems.Add(userOrderItem);
                    }
                    pizzaOrderDTO.UserOrders.Add(userOrder);
                }
            }

            return pizzaOrderDTO;
        }

    }
}
