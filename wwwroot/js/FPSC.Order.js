﻿(function (r, $) {
    r.order = {
        init: function () {
            /// <summary>
            /// Initialisation de l'interface
            /// </summary>

            r.init();
            initParticipants();
            initSubmitHandler();
        }
    };

    function initParticipants() {
        let participantsList = $("#participants");

        //Event : Delete participant
        handleDeleteParticipantEvents(participantsList);

        //Event : Add participant
        handleAddParticipant(participantsList);

        //Event : Delete user order item
        handleDeleteUserOrderItemEvents(participantsList);

        //Event : Add user order item
        handleAddUserOrderItem(participantsList);
    }

    function handleAddParticipant(parent) {
        /// <summary>
        /// Gestion de l'évènement "Add participant"
        /// </summary>

        var templateUserOrder = $("#template-UserOrder").html();
        $(parent).find("div[data-action='addParticipant']").click(function () {
            let participantsList = $("#participantsList");
            let newIdxParticipant = participantsList.find("div[class='participantOrder']").length;
            let newParticipant = $(templateUserOrder.replace(/_idxI_/g, newIdxParticipant));

            handleDeleteParticipantEvents(newParticipant);
            handleAddUserOrderItem(newParticipant);

            participantsList.append(newParticipant);
        });
    }

    function handleAddUserOrderItem(parent) {
        /// <summary>
        /// Gestion de l'évènement "Add user order item"
        /// </summary>

        var templateUserOrderItem = $("#template-UserOrderItem").html();
        $(parent).find("div[data-action='addUserOrderItem']").click(function () {
            let participantsList = $("#participantsList");
            let participant = $(this).closest("div[class='participantOrder']");
            let idxIParticipant = participantsList.find("div[class='participantOrder']").index(participant);
            let userOrders = participant.find("tbody");
            let newIdxUserOrderItem = userOrders.find("tr").length;
            let newUserOrderItem = $(templateUserOrderItem.replace(/_idxI_/g, idxIParticipant)
                .replace(/_idxJ_/g, newIdxUserOrderItem));

            handleDeleteUserOrderItemEvents(newUserOrderItem);

            userOrders.append(newUserOrderItem);
        });
    }

    function handleDeleteParticipantEvents(parent) {
        /// <summary>
        /// Gestion de l'évènement "Delete participant"
        /// </summary>

        $(parent).find("div[data-action='deleteParticipant']").click(function () {
            let participant = $(this).closest("div[class='participantOrder']");
            participant.remove();
        });
    }

    function handleDeleteUserOrderItemEvents(parent) {
        /// <summary>
        /// Gestion de l'évènement "Delete user order item"
        /// </summary>

        $(parent).find("div[data-action='deleteUserOrderItem']").click(function () {
            let userOrderItem = $(this).closest("tr");
            userOrderItem.remove();
        });
    }

    function initSubmitHandler() {
        /// <summary>
        /// Gestion de l'évènement submit
        /// </summary>

        $('form').on("submit", function (event) {
            let participantsList = $("#participantsList");

            //Ajustement des index pour ASP.NET MVC
            $("div[class='participantOrder']").each(function () {

                let participant = $(this);
                let idxIParticipant = participantsList.find("div[class='participantOrder']").index(participant);

                $(this).find('input, select').each(function () {
                    let name = $(this).attr("name").replace(/\UserOrders\[.+?\]/, "UserOrders[" + idxIParticipant + "]");
                    let userOrderItems = $(this).closest("tbody").find("tr");

                    if (userOrderItems.length) {
                        let userOrderItem = $(this).closest("tr");
                        let idxJUserOrderItem = userOrderItems.index(userOrderItem);
                        name = name.replace(/\OrderItems\[.+?\]/, "OrderItems[" + idxJUserOrderItem + "]");
                    }
                                                   
                    $(this).attr("name", name);
                });
                idxIParticipant = idxIParticipant + 1;
            });
        });
    }
})(window.fpsc = window.fpsc || {}, jQuery);