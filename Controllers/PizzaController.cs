﻿using System.Collections.Generic;
using FPSC.Data.DTO;
using FPSC.Data.Models;
using FPSC.PizzaService;
using Microsoft.AspNetCore.Mvc;

namespace FPSC.Controllers
{
    [Route("api/[controller]")]
    public class PizzaController : Controller
    {
        private IPizzaService _pizzaService;

        public PizzaController(IPizzaService pizzaService)
        {
            _pizzaService = pizzaService;
        }

        [HttpGet("menu")]
        public List<PizzaMenuItemDTO> GetMenu() => _pizzaService.GetMenu();

        [HttpGet("order/{id:int}")]
        public ResultData<PizzaOrderDTO> GetPizzaOrder(int id) => _pizzaService.GetPizzaOrder(id);

        [HttpPost("order")]
        public ResultData<PizzaOrderDTO> CreatePizzaOrder() => _pizzaService.CreatePizzaOrder();

        [HttpPost("order/{id:int}/userOrder")]
        public Result AddPizzaOrderItem(int id, [FromBody] PizzaUserOrderDTO order) => _pizzaService.AddPizzaUserOrder(id, order);

    }
}
