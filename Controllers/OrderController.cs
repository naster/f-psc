﻿using FPSC.Data.DTO;
using FPSC.Models;
using FPSC.PizzaService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FPSC.Controllers
{
    public class OrderController : Controller
    {
        private IPizzaService _pizzaService;

        public OrderController(IPizzaService pizzaService)
        {
            _pizzaService = pizzaService;
        }

        public IActionResult Index()
        {
            //Readonly data
            ViewData["MenuItemsGrouped"] = _pizzaService.GetMenuGroupedByPizzaType();
            ViewData["PizzaTypes"] = _pizzaService.GetPizzaTypes(); //For partial view

            var order = new PizzaOrderDTO
            {
                Date = DateTime.Now,
                UserOrders = new List<PizzaUserOrderDTO>()
            };

            //order.UserOrders = new List<PizzaUserOrderDTO>
            //{
            //    new PizzaUserOrderDTO { UserName = "Bob", OrderItems = new List<PizzaUserOrderItemDTO>
            //    {
            //        new PizzaUserOrderItemDTO{ PizzaTypeId = 1, PizzaTypeName = "Meat lover", NumberOfSlices = 8 },
            //        new PizzaUserOrderItemDTO{ PizzaTypeId = 1, PizzaTypeName = "Meat lover", NumberOfSlices = 16 },
            //        new PizzaUserOrderItemDTO{ PizzaTypeId = 1, PizzaTypeName = "Meat lover", NumberOfSlices = 32 }
            //    } },
            //    new PizzaUserOrderDTO { UserName = "Catherine", OrderItems = new List<PizzaUserOrderItemDTO>
            //    {
            //        new PizzaUserOrderItemDTO{ PizzaTypeId = 1, PizzaTypeName = "Meat lover", NumberOfSlices = 4 },
            //        new PizzaUserOrderItemDTO{ PizzaTypeId = 1, PizzaTypeName = "Meat lover", NumberOfSlices = 8 },
            //        new PizzaUserOrderItemDTO{ PizzaTypeId = 1, PizzaTypeName = "Meat lover", NumberOfSlices = 2 }
            //    } },
            //    new PizzaUserOrderDTO { UserName = "Jaques", OrderItems = new List<PizzaUserOrderItemDTO>
            //    {
            //        new PizzaUserOrderItemDTO{ PizzaTypeId = 1, PizzaTypeName = "Meat lover", NumberOfSlices = 8 },
            //        new PizzaUserOrderItemDTO{ PizzaTypeId = 1, PizzaTypeName = "Meat lover", NumberOfSlices = 9 },
            //        new PizzaUserOrderItemDTO{ PizzaTypeId = 1, PizzaTypeName = "Meat lover", NumberOfSlices = 2 }
            //    } },
            //};

            return View(order);
        }

        [HttpPost]
        public IActionResult Summary(PizzaOrderDTO pizzaOrderDTO)
        {
            var summary = new SummaryViewModel();

            var menuItemsGrouped = _pizzaService.GetMenuGroupedByPizzaType();
            var pizzaTypes = _pizzaService.GetPizzaTypes();

            //Readonly data
            ViewData["MenuItemsGrouped"] = menuItemsGrouped;
            ViewData["PizzaTypes"] = pizzaTypes; //For partial view

            //Make order
            OrderPizza(pizzaOrderDTO);

            //Get best price value
            var nbSlicesByPizzaType = from order in pizzaOrderDTO.UserOrders
                                      from orderItem in order.OrderItems
                                      group orderItem by orderItem.PizzaTypeId into itemGroup
                                      select new
                                      {
                                          PizzaTypeName = pizzaTypes.FirstOrDefault(t=>t.PizzaTypeId == itemGroup.Key).Name,
                                          TotalSlices = itemGroup.Sum(i => i.NumberOfSlices)
                                      };

            var menu = _pizzaService.GetMenuGroupedByPizzaType();

            summary.PizzasToOrder = new List<PizzaToOrder>();
            foreach (var pizzaType in nbSlicesByPizzaType)
            {
                var priceBySize = from menuItem in menu.Where(m => m.Key == pizzaType.PizzaTypeName)
                                  from menuSize in menuItem
                                  select new PizzaToOrder
                                  {
                                      Quantity = (int)Math.Ceiling((decimal)pizzaType.TotalSlices / menuSize.PizzaSizeNumberOfSlices),
                                      MenuItem = menuSize,
                                      NumberOfSlicesRequested = pizzaType.TotalSlices
                                  };
                //Save best value
                var pizzaToOrder = priceBySize.OrderBy(p => p.TotalEstimatedPrice).FirstOrDefault();
                summary.PizzasToOrder.Add(pizzaToOrder);
            }

            //Users summary
            summary.SliceByPeople = pizzaOrderDTO.UserOrders.SelectMany(uo => uo.OrderItems.Select(i => new SliceByPerson
            {
                UserName = uo.UserName,
                PizzaTypeName = pizzaTypes.FirstOrDefault(t => t.PizzaTypeId == i.PizzaTypeId).Name,
                NumberOfSlicesRequested = i.NumberOfSlices
            })).ToList();

            return View(summary);
        }

        private void OrderPizza(PizzaOrderDTO pizzaOrderDTO)
        {
            var pizzaOrder = _pizzaService.CreatePizzaOrder();
            if (!pizzaOrder.Success)
                throw new NotImplementedException();

            foreach (var order in pizzaOrderDTO.UserOrders)
                _pizzaService.AddPizzaUserOrder(pizzaOrder.Data.PizzaOrderId, order);
        }
    }
}
