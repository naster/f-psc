﻿using Microsoft.AspNetCore.Mvc;

namespace FPSC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
