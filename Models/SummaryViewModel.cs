﻿using FPSC.Data.DTO;
using System.Collections.Generic;
using System.Linq;

namespace FPSC.Models
{
    public class SummaryViewModel
    {
        public PizzaOrderDTO PizzaOrder { get; set; }
        public List<PizzaToOrder> PizzasToOrder { get; set; }
        public decimal GrandTotal => PizzasToOrder.Sum(p => p.TotalEstimatedPrice);
        public List<SliceByPerson> SliceByPeople { get; set; }
        public int TotalUsers => SliceByPeople.Select(s => s.UserName).Distinct().Count();
    }
}
