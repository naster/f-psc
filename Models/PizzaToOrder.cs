﻿using FPSC.Data.DTO;
using FPSC.Data.Models;

namespace FPSC.Models
{
    public class PizzaToOrder
    {
        public PizzaMenuItemDTO  MenuItem { get; set; }
        public int Quantity { get; set; }
        public int NumberOfSlicesRequested { get; set; }
        public decimal TotalEstimatedPrice => MenuItem.Price * Quantity;
        public int NumberOfSlicesIncluded => MenuItem.PizzaSizeNumberOfSlices * Quantity;
    }
}
