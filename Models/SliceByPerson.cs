﻿namespace FPSC.Models
{
    public class SliceByPerson
    {
        public string PizzaTypeName { get; set; }
        public int NumberOfSlicesRequested { get; set; }
        public string UserName { get; set; }
    }
}
